{
  "namespace": "com.apple.thermal.schema",
  "type": "record",
  "name": "JobManifest",
  "doc": "v.1.0.1 - Specifies the artifacts created from a Thermal Icarus testing harness job.",
  "fields": [
    {
      "name": "id",
      "type": "string",
      "doc": "Key Id prefix for records and objects. Concatination of lowercased serialNumber +'-'+processTimestamp.getMillis(). Example dlxsp027hqg7-1484161113000"
    },
    {
      "name": "processTimestamp",
      "type": [
        "null",
        {
          "type": "long",
          "logicalType": "timestamp-millis"
        }
      ],
      "default": null,
      "doc": "DateTime of the IcarusTest Job."
    },
    {
      "name": "device",
      "type": "string",
      "doc": "Obtained from the first token in the name of the folder in the zip. Could get this from iTrack. Device is the unique immutable identifier of the type of device under test for this particular job. These are definitive with Thermal but do have relationship to iTrack. Alpha start letters code project designated.  Example: D53g D33 J207 J237 J211 N84 N11 D43 D42 B520"
    },
    {
      "name": "phase",
      "type": "string",
      "doc": "From iTrack. Hardware build for a specfic device.  Mix of letters and numbers. Can coorelate to a config of a device. May relate to iTrack record. Example: P2 TTV EVT DVT CRB XRB PVT EFFA SHIPPING PROTO"
    },
    {
      "name": "serialNumber",
      "type": "string",
      "doc": "Serial number is the unique immutable identifier of the device under test. Example: G6TC5013PT3L"
    },
    {
      "name": "unitNumber",
      "type": "string",
      "doc": "From iTrack. Need description"
    },
    {
      "name": "os",
      "type": "string",
      "doc": "Internal Apple build number for a daily OS Train build.  For a particular serial numbered device this is a muttable value and will change at least weekly. Example: 18A205"
    },
    {
      "name": "uploadedBy",
      "type": [
        "null",
        "int"
      ],
      "default": null,
      "doc": "TODO from AppleConnect. Employee Id of Test Engineer that uploaded the archive."
    },
    {
      "name": "deCreateTs",
      "type": [
        "null",
        {
          "type": "long",
          "logicalType": "timestamp-millis"
        }
      ],
      "doc": "Data Engineering creation time."
    },
    {
      "name": "settings",
      "type": {
        "name": "Settings",
        "doc": "The OS settings of a device under test. Set by the operator via ssh into the device or Icarus via its device agent. Note all internal OS builds of devices have SSH functionality.",
        "type": "record",
        "fields": [
          {
            "name": "cltm",
            "type": [
              "null",
              "boolean"
            ],
            "default": null,
            "doc": "Closed Loop Thermal Monitor.  CLTM is the control loop for monitoring and controling thermal response of IoS devices. Whether the device responds to thermal notifications. Not customer settable. Default to null for MacOS"
          },
          {
            "name": "ccl",
            "type": [
              "null",
              "boolean"
            ],
            "default": null,
            "doc": "Candidate Control Loops. This enables the under test to use the future versions of the CLTM.  Not customer settable. Default to null for MacOS"
          },
          {
            "name": "airplaneMode",
            "type": [
              "null",
              "boolean"
            ],
            "default": null,
            "doc": "Turns off all radio.  Default to null for MacOS"
          },
          {
            "name": "wifi",
            "type": [
              "null",
              "boolean"
            ],
            "default": null,
            "doc": "Enable Wifi.  Default to null for devices without Wifi"
          },
          {
            "name": "als",
            "type": [
              "null",
              "boolean"
            ],
            "default": null,
            "doc": "Ambient Light sensors."
          },
          {
            "name": "autoLock",
            "type": [
              "null",
              "boolean"
            ],
            "default": null,
            "doc": "When true device will not lock the screen."
          },
          {
            "name": "icarusVersion",
            "type": "string",
            "doc": "Exmple 4.19.1 "
          },
          {
            "name": "bootArgs",
            "type": "string",
            "doc": "Space seperated strings as submitted to the device via ssh.  Environmental variables for the boot sequence of the device placed in non-volitale memory befor a reboot of the device. Exmple amfi_allow_bni_as_platform=1 amfi_get_out_of_my_way=1 launchctl_enforce_codesign=0 debug=0x104c04"
          }
        ]
      }
    },
    {
      "name": "tests",
      "type": {
        "type": "array",
        "doc": "TODO add duration. TODO need validation and descriptions of these field names",
        "items": {
          "name": "TestRecord",
          "type": "record",
          "fields": [
            {
              "name": "startTime",
              "type": [
                "null",
                {
                  "type": "int",
                  "logicalType": "time-millis"
                }
              ],
              "doc": "Example: 04:24:02 PM"
            },
            {
              "name": "duration",
              "type": [
                  "null",
                  "string"
              ],
              "default": null,
              "doc": "Example: 60 min."
            },
            {
              "name": "testName",
              "type": "string",
              "doc": "Example 1080p30"
            },
            {
              "name": "testNumber",
              "type": "int"
            },
            {
              "name": "cltm",
              "type": [
                "null",
                "boolean"
              ],
              "default": null,
              "doc": "Closed Loop Thermal Monitor.  CLTM is the control loop for monitoring and controling thermal response of IoS devices. Whether the device responds to thermal notifications. Not customer settable. Default to null for MacOS"
            },
            {
              "name": "ccl",
              "type": [
                "null",
                "boolean"
              ],
              "default": null,
              "doc": "Candidate Control Loops. This enables the under test to use the future versions of the CLTM.  Not customer settable. Default to null for MacOS"
            },
            {
              "name": "volume",
              "type": [
                "null",
                "float"
              ],
              "default": null
            },
            {
              "name": "battery",
              "type": "string"
            },
            {
              "name": "backlight",
              "type": "float"
            },
            {
              "name": "cpu",
              "type": "string"
            },
            {
              "name": "gpu",
              "type": "string"
            },
            {
              "name": "call",
              "type": "string"
            },
            {
              "name": "wiFiTest",
              "type": "string"
            },
            {
              "name": "app",
              "type": "string"
            },
            {
              "name": "ambientTemp",
              "type": [
                "null",
                "string"
              ],
              "default": null,
              "doc": "Ambient temperature during test."
            }
           
          ]
        }
      }
    }
  ]
}